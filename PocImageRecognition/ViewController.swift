//
//  ViewController.swift
//  PocImageRecognition
//
//  Created by Alex Ech on 9/17/19.
//  Copyright © 2019 Alex Ech. All rights reserved.
//

import UIKit
import CoreML
import Vision
import ImageIO

class ViewController: UIViewController {

  @IBOutlet weak var previewImageView: UIImageView!
  @IBOutlet weak var resultTextView: UITextView!
  @IBOutlet weak var modelButton: UIButton!
  @IBOutlet weak var cameraButton: UIButton!
  @IBOutlet weak var galleryButton: UIButton!

  /*lazy var classificationRequest: VNCoreMLRequest = {
    do {
      let model = try VNCoreMLModel(for: self.setupModel(name: self.modelName))
      let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
        self?.processClassifications(for: request, error: error)
      })
      // request.imageCropAndScaleOption = .original
      return request
    } catch {
      fatalError("Failed to load Vision ML model: \(error)")
    }
  }()*/

  private var currentImage: UIImage?
  private var modelName = "resnet"
  private var modelPickerView: UIPickerView = UIPickerView()
  private var prickerElements: [String] = [
    "resnet",
    "mudpie",
    "yolo",
    "mobile",
    "squeeze",
    "pokemon",
    "Cancel"
  ]

  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupModelName()
    self.setupModelPickerView()
  }

  @IBAction func selectModel(_ sender: Any) {
    if self.currentImage == nil {
      let alertController = UIAlertController(title: "", message: "Select an image", preferredStyle: .alert)
      alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
      self.present(alertController, animated: true, completion: nil)
      return
    }
    self.modelButton.isEnabled = false
    self.modelButton.alpha = 0.5
    self.modelPickerView.isHidden = false
  }

  @IBAction func openCamera(_ sender: Any) {
    self.photoLibraryAuthorization(to: "camera")
  }

  @IBAction func openGallery(_ sender: Any) {
    self.photoLibraryAuthorization(to: "gallery")
  }

  /// - Tag: PerformRequests
  private func updateClassifications(for image: UIImage) {
    self.resultTextView.text = "Classifying..."
    self.modelButton.isEnabled = false
    self.modelButton.alpha = 0.5
    self.cameraButton.isEnabled = false
    self.cameraButton.alpha = 0.5
    self.galleryButton.isEnabled = false
    self.galleryButton.alpha = 0.5
    guard
      let orientation = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue)),
      let ciImage = CIImage(image: image),
      let classification = self.classificationRequest() else {
        self.alertError()
        return
      }

    DispatchQueue.global(qos: .userInitiated).async {
      let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation)
      do {
        try handler.perform([classification])
      } catch {
        /*
         This handler catches general image processing errors. The `classificationRequest`'s
         completion handler `processClassifications(_:error:)` catches errors specific
         to processing that request.
         */
        print("Failed to perform classification.\n\(error.localizedDescription)")
      }
    }
  }

  /// Updates the UI with the results of the classification.
  /// - Tag: ProcessClassifications
  private func processClassifications(for request: VNRequest, error: Error?) {
    DispatchQueue.main.async {
      guard let results = request.results else {
        self.resultTextView.text = "Unable to classify image.\n\(error!.localizedDescription)"
        return
      }
      // The `results` will always be `VNClassificationObservation`s, as specified by the Core ML model in this project.
      if let classifications = results as? [VNClassificationObservation] {
        self.outputClassification(classifications)
      }

      if let recognizeds = results as? [VNRecognizedObjectObservation] {
        if recognizeds.isEmpty {
          self.resultTextView.text = "Nothing recognized."
        } else {
          let topRecognizeds = recognizeds.prefix(2)
          if let firstDescription = topRecognizeds.first {
            self.outputClassification(firstDescription.labels)
          }
        }
      }
    }
  }

  private func outputClassification(_ classifications: [VNClassificationObservation]) {
    self.modelButton.isEnabled = true
    self.modelButton.alpha = 1
    self.cameraButton.isEnabled = true
    self.cameraButton.alpha = 1
    self.galleryButton.isEnabled = true
    self.galleryButton.alpha = 1
    if classifications.isEmpty {
      self.resultTextView.text = "Nothing recognized."
    } else {
      print("classifications: ", classifications)
      // Display top classifications ranked by confidence in the UI.
      let topClassifications = classifications.prefix(2)
      let descriptions = topClassifications.map { classification in
        // Formats the classification for display; e.g. "(0.37) cliff, drop, drop-off".
        return String(format: " › %.0f%% %@", (classification.confidence * 100), classification.identifier).uppercased()
      }
      self.resultTextView.text = "Result:\n" + descriptions.joined(separator: "\n")
    }
  }

  private func photoLibraryAuthorization(to service: String) {
    switch service {
    case "camera":
      self.openCameraController()
    case "gallery":
      self.openGalleryController()
    default:
      break
    }
  }

  private func openCameraController() {
    DispatchQueue.main.async {
      if UIImagePickerController.isSourceTypeAvailable(.camera) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .camera
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
      }
    }
  }

  private func openGalleryController() {
    DispatchQueue.main.async {
      if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.allowsEditing = false
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
      }
    }
  }

  private func setupModel(name: String) -> MLModel {
    switch name {
    case "mudpie":
      return Mudpie().model
    case "yolo":
      return YOLOv3().model
    case "resnet":
      return Resnet50().model
    case "squeeze":
      return SqueezeNet().model
    case "pokemon":
      return PokemonClassifier().model
    case "mobile":
      return MobileNetV2().model
    default:
      self.modelName = "default"
      self.setupModelName()
      return MobileNetV2().model
    }
  }

  private func setupModelName() {
    DispatchQueue.main.async {
      self.modelButton.setTitle(self.modelName, for: .normal)
      self.modelButton.setTitle(self.modelName, for: .selected)
    }
  }

  private func setupModelPickerView() {
    self.modelPickerView.isHidden = true
    self.modelPickerView.delegate = self
    self.modelPickerView.dataSource = self
    self.modelPickerView.backgroundColor = .white
    self.modelPickerView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
    self.modelPickerView.layer.borderWidth = 1
    self.modelPickerView.frame = CGRect(
      x: 0,
      y:  self.view.frame.maxY - 175,
      width: self.view.frame.width,
      height: 175
    )
    self.view.addSubview(self.modelPickerView)
  }

  private func classificationRequest() ->  VNCoreMLRequest? {
    do {
      let model = try VNCoreMLModel(for: self.setupModel(name: self.modelName))
      let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
        self?.processClassifications(for: request, error: error)
      })
      // request.imageCropAndScaleOption = .original
      return request
    } catch {
      self.alertError()
      debugPrint("[classificationRequest]: Failed to load Vision ML model: \(error)")
      return nil
    }
  }

  private func alertError() {
    DispatchQueue.main.async {
      self.resultTextView.text = "Failed"
    }
    let alertController = UIAlertController(title: "Error", message: "Failed recognition.", preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
    self.present(alertController, animated: true, completion: nil)
  }
}

extension ViewController: UIImagePickerControllerDelegate,
UINavigationControllerDelegate {

  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
    self.dismiss(animated: true, completion: nil)
    guard let image = info[.originalImage] as? UIImage,
      let resizeImage = image.resized(toWidth: 1024) else {
        return
    }
    self.currentImage = resizeImage
    self.previewImageView.image = resizeImage
    self.updateClassifications(for: resizeImage)
  }

  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    self.dismiss(animated: true, completion: nil)
  }
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }

  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return self.prickerElements.count
  }

  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return self.prickerElements[row]
  }

  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    self.modelPickerView.isHidden = true
    guard self.prickerElements[row] != "Cancel" else {
      self.modelButton.isEnabled = true
      self.modelButton.alpha = 1
      return
    }
    self.modelName = self.prickerElements[row]
    self.setupModelName()
    if let image = self.currentImage {
      self.updateClassifications(for: image)
    }
  }
}

extension UIImage {
  func resized(toWidth width: CGFloat) -> UIImage? {
    let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
    UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
    defer { UIGraphicsEndImageContext() }
    draw(in: CGRect(origin: .zero, size: canvasSize))
    return UIGraphicsGetImageFromCurrentImageContext()
  }
}
